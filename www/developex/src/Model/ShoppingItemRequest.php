<?php

namespace App\Model;

class ShoppingItemRequest
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ShoppingItemRequest
     */
    public function setId(?int $id): ShoppingItemRequest
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ShoppingItemRequest
     */
    public function setTitle(string $title): ShoppingItemRequest
    {
        $this->title = $title;

        return $this;
    }
}
