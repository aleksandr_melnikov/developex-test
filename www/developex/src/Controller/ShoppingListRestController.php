<?php

namespace App\Controller;

use App\Entity\ShoppingItem;
use App\Model\ShoppingItemRequest;
use App\Repository\ShoppingItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface as SerializerExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ShoppingListRestController extends AbstractController
{
    /**
     * @var ShoppingItemRepository
     */
    private $shoppingItemRepository;

    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        ShoppingItemRepository $shoppingItemRepository,
        NormalizerInterface $normalizer,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->shoppingItemRepository = $shoppingItemRepository;
        $this->normalizer = $normalizer;
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @return JsonResponse
     */
    public function getListAction()
    {
        $shoppingListItems = $this->shoppingItemRepository->findAll();

        return new JsonResponse(
            $this->normalizer->normalize($shoppingListItems)
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateAction(Request $request)
    {
        try {
            $shoppingItemRequest = $this->serializer
                ->deserialize($request->getContent(), ShoppingItemRequest::class, 'json');

            $violationList = $this->validator->validate($shoppingItemRequest);

            if ($violationList->count() !== 0) {
                return $this->jsonValidationErrorResponse($violationList);
            }

            if ($shoppingItemRequest->getId()) {
                $shoppingListItem = $this->shoppingItemRepository->getById(
                    $shoppingItemRequest->getId()
                );
            } else {
                $shoppingListItem = new ShoppingItem();
                $this->entityManager->persist($shoppingListItem);
            }

            $shoppingListItem->setTitle($shoppingItemRequest->getTitle());
            $this->entityManager->flush();

            return $this->jsonOkResponse($shoppingListItem);
        } catch (\Throwable $e) {
            return new JsonResponse(['errorMessage' => $e->getMessage()], 400);
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function removeAction(int $id)
    {
        try {
            $shoppingItem = $this->shoppingItemRepository->find($id);
            $this->entityManager->remove($shoppingItem);
            $this->entityManager->flush();

            $response = $this->jsonOkResponse();
        } catch (\Throwable $e) {
            $response = $this->jsonErrorResponse($e);
        }

        return $response;
    }

    private function jsonErrorResponse(\Throwable $throwable)
    {
        switch (true) {
            case $throwable instanceof SerializerExceptionInterface:
                $responseCode =  JsonResponse::HTTP_BAD_REQUEST;
                break;
            case $throwable instanceof NoResultException:
                $responseCode =  JsonResponse::HTTP_NOT_FOUND;
                break;
            default:
                $responseCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse(
            ['errorMessage' => $throwable->getMessage()],
            $responseCode
        );
    }

    /**
     * @param ShoppingItem|null $shoppingItem
     * @return JsonResponse
     */
    private function jsonOkResponse(?ShoppingItem $shoppingItem = null)
    {
        return new JsonResponse(
            $shoppingItem ? $this->normalizer->normalize($shoppingItem) : '',
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @param ConstraintViolationListInterface $list
     * @return JsonResponse
     */
    private function jsonValidationErrorResponse(
        ConstraintViolationListInterface $list
    ): JsonResponse {

            /**
             * @var $item ConstraintViolationInterface
             */
            foreach ($list as $item) {
                $message[] = [
                    'propertyPath' => $item->getPropertyPath(),
                    'error' => $item->getMessage(),
                ];
            }

            return new JsonResponse(
                ['message' => $message],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
