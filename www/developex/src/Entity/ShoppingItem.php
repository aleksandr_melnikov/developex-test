<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingItemRepository")
 * @ORM\Table(name="shopping_item")
 * @ORM\HasLifecycleCallbacks()
 */
class ShoppingItem
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    private $dateUpdated;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ShoppingItem
     */
    public function setTitle($title): ShoppingItem
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateCreated(): \DateTimeInterface
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTimeInterface $dateCreated
     * @return ShoppingItem
     */
    public function setDateCreated(\DateTimeInterface $dateCreated): ShoppingItem
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateUpdated(): \DateTimeInterface
    {
        return $this->dateUpdated;
    }

    /**
     * @param \DateTime $dateUpdated
     * @return ShoppingItem
     */
    public function setDateUpdated(\DateTimeInterface $dateUpdated): ShoppingItem
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist(){
        $this->setDateCreated(new \DateTime());
        $this->setDateUpdated(new \DateTime());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setDateUpdated(new \DateTime());
    }
}
