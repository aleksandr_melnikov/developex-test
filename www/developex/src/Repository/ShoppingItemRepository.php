<?php

namespace App\Repository;

use App\Entity\ShoppingItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ShoppingItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, ShoppingItem::class);
    }

    public function getById(int $id)
    {
        return $this->getEntityManager()
                    ->createQueryBuilder()
                    ->select('shoppingItem')
                    ->from(ShoppingItem::class, 'shoppingItem')
                    ->andWhere('shoppingItem.id=:id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getSingleResult();
    }
}
