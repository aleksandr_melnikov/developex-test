import { combineReducers } from 'redux';
import shoppingListReducer from '../components/shoppingList/shoppingList.reducer'

export default combineReducers({
    shoppingListReducer: shoppingListReducer
});
