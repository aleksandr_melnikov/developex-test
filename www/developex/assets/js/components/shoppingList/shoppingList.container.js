import {connect} from 'react-redux';

import ShoppingListView from './shoppingList.view';
import {addItemAction, editItemAction, addItemAsyncAction, removeItemAction, removeItemAsyncAction, editItemAsyncAction} from './shoppingList.reducer';

export const mapStateToProps = ({ shoppingListReducer }) => {
        return {
            itemList: shoppingListReducer.itemList
        }
    };

export const mapDispatchToProps = (dispatch) => (
    {
        addItemAction: (payload) => dispatch(addItemAction(payload)),
        addItemAsyncAction: (payload) => dispatch(addItemAsyncAction(payload)),
        editItemAction: (payload) => dispatch(editItemAction(payload)),
        editItemAsyncAction: (payload) => dispatch(editItemAsyncAction(payload)),
        removeItemAction: (payload) => dispatch(removeItemAction(payload)),
        removeItemAsyncAction: (payload) => dispatch(removeItemAsyncAction(payload))
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingListView);
