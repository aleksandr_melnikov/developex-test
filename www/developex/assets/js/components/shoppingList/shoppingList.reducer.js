export const addItemAction = (payload) => (
    {
        type: 'ADD_ITEM',
        payload: payload
    }
);

export const editItemAction = (payload) => (
    {
        type: 'EDIT_ITEM',
        payload: payload,
    }
);

export const removeItemAction = (payload) => (
    {
        type: 'REMOVE_ITEM',
        payload: payload,
    }
);

export const itemsFetchedAction = (payload) => (
    {
        type: 'FETCH_ITEMS',
        payload: payload
    }
);

export const fetchItemsAsyncAction = () => {

    return function (dispatch) {

        fetch('http://developex-test.loc/shopping-list/items')
            .then(response => response.ok ? response.json() : null)
            .then(json => dispatch(itemsFetchedAction(json)));
    }
};

export const editItemAsyncAction = (payload) => {
    return function (dispatch) {
        fetch('http://developex-test.loc/shopping-list/item',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify(payload)
            })
            .then(response => response.ok ? response.json() : null)
            .then(json => json ? dispatch(editItemAction(json)) : null)
    }
};

export const addItemAsyncAction = (payload) => {

    return function (dispatch) {

        fetch('http://developex-test.loc/shopping-list/item',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({title: payload.title})
            })
            .then(response => response.ok ? response.json() : null)
            .then(json => json ? dispatch(addItemAction(json)) : null);

    }
};

export const removeItemAsyncAction = (payload) => {

    return function (dispatch) {

        fetch('http://developex-test.loc/shopping-list/item/' + payload.id,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "DELETE"
            })
            .then(response => response.ok ? response.json() : null)
            .then(json => json ? dispatch(removeItemAction({id: payload.id})) : null);
    }
};


export const initialState = {
    itemList: []
};

export default function shoppingListReducer(state = initialState, action) {
    switch (action.type) {
        case 'ADD_ITEM':
            const addNewState = {...state};
            addNewState.itemList = [...state.itemList, action.payload];

            return addNewState;

        case 'REMOVE_ITEMS':
            return state.filter((item) => item.id !== action.payload.id);

        case 'EDIT_ITEM':
            const newState = {...state};
            const key = newState.itemList.findIndex((item) => item.id === action.payload.id);
            newState.itemList[key] = action.payload;

            return newState;

        case 'FETCH_ITEMS':
            const newFetchState = {...state};
            newFetchState.itemList = [...action.payload];

            return newFetchState;

        default:
            return state;
    }
}
