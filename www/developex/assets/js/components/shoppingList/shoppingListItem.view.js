import React, {Component} from 'react';

export default class ShoppingListItemView extends Component {

    state = {
        item: this.props.item,
        inputTitle: this.props.item.title,
        editState: this.props.editState
    };
    onTextChangeHandler = (e) => {
        const key = e.currentTarget.getAttribute('name');
        const stateAttr = {};

        stateAttr[key] = e.currentTarget.value;
        this.setState(stateAttr);
    };
    onSaveClick = () => {
        const payload = {
            id: this.state.item.id,
            title: this.state.inputTitle
        };
        this.setState({
            editState: false
        });
        this.props.editItemAction(payload);
    };
    onEditClick = () => {
        this.setState({
            editState: true
        });
    };
    onRemoveClick = () => {
        const payload = {
            id: this.state.item.id,
        };

        this.props.removeItemAction(payload);
    };
    renderInputRow = () => {
        return <div>
            <input type="text" name="inputTitle" value={this.state.inputTitle} onChange={this.onTextChangeHandler}/>
            <button onClick={this.onSaveClick}>Save</button>
        </div>;
    };
    renderBlockRow = () => {
        return (
            <div>
                {this.props.item.title}
                <button className="button" onClick={this.onEditClick}>
                    Edit
                </button>
                <button className="button" onClick={this.onRemoveClick}>
                    Remove
                </button>
            </div>
        );
    }

    static getDerivedStateFromProps(props, state) {
        if (JSON.stringify(props.item) !== JSON.stringify(state.item)) {
            return {item: props.item, inputTitle: props.item.title}
        }

        return null;
    }

    render() {
        return this.state.editState ? this.renderInputRow() : this.renderBlockRow();
    }
}


