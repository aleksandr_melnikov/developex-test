import React, {Component, Fragment} from 'react';
import ShoppingListItemView from './shoppingListItem.view';

export default class ShoppingListView extends Component {
    state = {
        addBlockVisible: false
    };

    changeAddItemVisibleState = () => {
        this.setState({
            addBlockVisible: !this.state.addBlockVisible
        });
    };

    addItemAction = (payload) => {
        this.changeAddItemVisibleState();
        this.props.addItemAsyncAction(payload);
    };

    render() {
        const itemsBlock = this.props.itemList.map(item => {
            return <ShoppingListItemView key={item.id} editState={false} item={item}
                                         editItemAction={this.props.editItemAsyncAction}
                                         removeItemAction={this.props.removeItemAsyncAction}/>
        });

        const addBlock = this.state.addBlockVisible
            ? <ShoppingListItemView editState={true} item={{title: '', id: 'new'}}
                                    editItemAction={this.addItemAction}
                                    removeItemAction={this.changeAddItemVisibleState}/>
            : <button onClick={this.changeAddItemVisibleState}>Add new</button>;

        return (
            <Fragment>
                <div>
                    {addBlock}
                </div>
                <div className="shoppingList-wrapper">
                    {itemsBlock}
                </div>
            </Fragment>
        );
    }
}

