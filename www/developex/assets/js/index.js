import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {fetchItemsAsyncAction} from './components/shoppingList/shoppingList.reducer';
import ShoppingListView from './components/shoppingList/shoppingList.container';
import {applyMiddleware, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './redux/root-reducer';

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

ReactDOM.render(
    <Provider store={store}>
        <ShoppingListView/>
    </Provider>,

    document.getElementById('root')
);

setInterval(function () {
    store.dispatch(fetchItemsAsyncAction());
}, 1000);


