const path = require("path");

// Конфиг клиента.
clientConfig = {
    mode: "development",
    entry: {
        client: ["./assets/js/index.js"]
    },
    output: {
        path: path.resolve(__dirname, "./public/js"),
        filename: "app.js"
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
        ]
    }
};

module.exports = [clientConfig];
