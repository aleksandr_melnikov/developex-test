OS := $(shell uname)

start_dev:
	docker volume create --name=developex-sync
	docker-compose -f docker-compose.yml up -d --build
	docker-sync start

stop_dev:   ## Stop the Docker containers
	docker-compose -f docker-compose.yml stop
	docker-sync stop
